/*
 * Copyright 2016 CarIQ Technologies Pvt. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *  @author Basit Parkar
 *  @date 3/8/16 3:26 PM
 *  @modified 3/8/16 3:26 PM
 */

package me.iz.mobility.portableecg.utils;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ibasit
 */
public class Utils {

    private final String TAG = getClass().getSimpleName();

    final private static char[] hexArray = "0123456789ABCDEF".toCharArray();


    public static byte[] getItsBytes(String hexStr) {
        byte[] byteArr;
        try {
            byteArr = Hex.decodeHex(hexStr.toCharArray());
        } catch (DecoderException e) {
            throw new IllegalArgumentException(e);
        }
//        List<Byte> listOfBytes = new ArrayList<>();
//        for (byte b : byteArr) {
//            listOfBytes.add(b);
//        }
        return byteArr;
    }


    public static String getItsHex(byte[] byteArr) {

        String hexStr;
        hexStr = new String(Hex.encodeHex(byteArr));
        return hexStr;
    }

    public static char[] getItsIntArr(String hexString) {
        return hexString.toCharArray();
    }

    public static String getItsString(char[] arr) {
        return new String(arr);
    }


    public static String calculateCrc(String data) {
        int buff_index = 0;
        int crc = 0xFFFF;

        byte[] pay_load_crc = getItsBytes(data);
        int length = pay_load_crc.length;

        while (length > 0) {
            length--;
            crc ^= pay_load_crc[buff_index++] & 0xFF;

            for (int i = 0; i < 8; i++) {
                if ((crc & 0x0001) == 1) {
                    crc = (crc >> 1) ^ 0xa001;
                } else {
                    crc = crc >> 1;
                }
            }
        }

        return String.format("%02x", crc);

//        return Long.toHexString(crc);
    }

    public static int getIntFromHex(String hex) {
        try {
            return Integer.parseInt(hex, 16);
        } catch(Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getHexFromint(int val) {
        return String.format("%02x", val);
    }

    public static String getHexint(int val) {
        return String.format("%08x", val);
    }


    public static float convertValue(String value, String scale) {

        int convertedValue = getIntFromHex(value);
        int convertedScalar = getIntFromHex(scale);

        return convertedValue / (float) (Math.pow(10, convertedScalar));

    }


    public static float convertValue(int convertedValue, int convertedScalar) {
        return convertedValue / (float) (Math.pow(10, convertedScalar));
    }

    public static List<String> split(String originalString, int length) {

        List<String> mList = new ArrayList<>();
        int startSize = 0;
        for (int i = 0; i < (originalString.length() / length); i++) {
            int endSize = startSize + length;
            mList.add(originalString.substring(startSize, endSize));
            startSize = startSize + length;
        }

        return mList;
    }

    /**
     * Method to get floating values from hex strings
     *
     * @param arr
     * @return
     */
    public static float[] getConvertedValues(List<String> arr) {

        float[] values = new float[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            String s = arr.get(i);
            float flValue = getConvertedValue(s);
            values[i] = flValue;
        }

        return values;
    }


    public static float getConvertedValue(String s) {

//        Timber.d("Finding value of %s", s);
        String value = s.substring(0, s.length() - 2);
        String scale = s.substring(s.length() - 2, s.length());

        float flValue = convertValue(value, scale);
//        Timber.i("Final value %s", flValue);
        return flValue;
    }

    /**
     * Get Integer values directly for harmonics
     *
     * @param arr
     * @return
     */
    public static int[] getIntegerValues(List<String> arr) {

        int[] values = new int[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            String s = arr.get(i);
//            Timber.d("Finding value of %s", s);

            int flValue = getIntFromHex(s);
//            Timber.i("Final value %s", flValue);
            values[i] = flValue;
        }

        return values;
    }


    /**
     * Gets jex stromg from integer array
     * @param arr
     * @return
     */
    public static String getStringValue(int[] arr) {

        StringBuilder sb = new StringBuilder();

        for (int anArr : arr) {
            String s = getHexFromint(anArr);
            sb.append(s);
        }

        return sb.toString();
    }

    public static String asciiToHex(String asciiValue) {
        char[] chars = getItsIntArr(asciiValue);

        StringBuilder hex = new StringBuilder();
        for (char aChar : chars) {
            hex.append(Integer.toHexString((int) aChar));
        }
        return hex.toString();
    }


    public static String hexToAscii(String hexValue) {

        byte[] hexByte = getItsBytes(hexValue);

        String result = null;
        try {
            result = new String(hexByte,"ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result;

    }

}
