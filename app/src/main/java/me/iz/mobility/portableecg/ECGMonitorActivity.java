/*
 * Copyright 2016 CarIQ Technologies Pvt. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *  @author Basit Parkar
 *  @date 2/23/16 10:15 PM
 *  @modified 2/23/16 10:02 PM
 */

package me.iz.mobility.portableecg;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;
import butterknife.Bind;
import butterknife.OnClick;
import me.iz.mobility.portableecg.utils.Utils;

public class ECGMonitorActivity extends BaseBluetoothActivity {

    private static final String TAG = "ECGMonitorActivity";

    @Bind(R.id.realtimeChart)
    LineChart mChart;

    @Bind(R.id.btnConnect)
    Button btnConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecgmonitor);

        initChart();
    }


    private void dataReceiver() {

        Log.d(TAG, "dataReceiver: started");
        btHandle.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            @Override
            public void onDataReceived(byte[] data, String message) {

                Log.d(TAG, "Data received " + message);
                String val = Utils.getItsHex(data);

                Log.d(TAG, "Data received " + message + "-- "+val+ "-"+Utils.getIntFromHex(val));
                addEntry(Utils.getIntFromHex(val));
            }
        });
    }

    private void connectionCallbacks() {
        btHandle.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            @Override
            public void onDeviceConnected(String name, String address) {
                Log.d(TAG, "Connected to " + name + " : " + address);
//                TODO Start sending messages
                btnConnect.setVisibility(View.GONE);
//                send(TOU_COMMAND);
            }

            @Override
            public void onDeviceDisconnected() {
                Log.w(TAG, "onDeviceDisconnected: ");

                Toast.makeText(getApplicationContext()
                        , "Bluetooth disconnected"
                        , Toast.LENGTH_SHORT).show();

                if(btnConnect != null)
                    btnConnect.setVisibility(View.VISIBLE);
            }

            @Override
            public void onDeviceConnectionFailed() {
                Log.d(TAG, "Device connection failed");
                Toast.makeText(getApplicationContext()
                        , "Bluetooth connection failed"
                        , Toast.LENGTH_SHORT).show();

            }
        });
    }


    private void initChart() {
        mChart.setDescription("");
        mChart.setNoDataTextDescription("You need to provide data for the chart.");

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
//        int color = ContextCompat.getColor(getActivity(), android.R.color.transparent);
//        mChart.setBackgroundColor(color);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        mChart.setBackgroundColor(Color.BLACK);

        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        // add empty data
        mChart.setData(data);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.WHITE);

        XAxis xl = mChart.getXAxis();
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        xl.setSpaceBetweenLabels(5);
        xl.setEnabled(true);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setAxisMaxValue(100f);
        leftAxis.setAxisMinValue(0f);
        leftAxis.setStartAtZero(true);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        //feedMultiple();
    }


    private LineDataSet createSet() {

        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(4f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        return set;
    }

    private void addEntry(int value) {

        LineData data = mChart.getData();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            // add a new x-value first
            data.addXValue("");
            data.addEntry(new Entry((float) value, set.getEntryCount()), 0);


            // let the chart know it's data has changed
            mChart.notifyDataSetChanged();

            // limit the number of visible entries
            mChart.setVisibleXRangeMaximum(120);
            // mChart.setVisibleYRange(30, AxisDependency.LEFT);

            // move to the latest entry
            mChart.moveViewToX(data.getXValCount() - 121);

            // this automatically refreshes the chart (calls invalidate())
            // mChart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
        }
    }

    private void feedMultiple() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < 500; i++) {

                    final int finalTemp = i;
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            addEntry((finalTemp * 100)%60);
                        }
                    });

                    try {
                        Thread.sleep(35);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


    @OnClick(R.id.btnConnect)
    public void onClickConnect() {
//        btHandle.autoConnect("HC-05");

        if (btHandle.getServiceState() == BluetoothState.STATE_CONNECTED) {
            btHandle.disconnect();
        } else {
            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
            startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        }

//        autoConnectListener();
        dataReceiver();

        connectionCallbacks();

        btStateChange();
    }

    private void autoConnectListener() {

        btHandle.setAutoConnectionListener(new BluetoothSPP.AutoConnectionListener() {
            public void onNewConnection(String name, String address) {
                // Do something when earching for new connection device

                Log.d(TAG, "Connected to " + name + ":" + address);
            }

            public void onAutoConnectionStarted() {
                // Do something when auto connection has started
                Log.d(TAG, "Auto connection started");
            }
        });
    }


    private void btStateChange() {
        btHandle.setBluetoothStateListener(new BluetoothSPP.BluetoothStateListener() {
            public void onServiceStateChanged(int state) {
                if (state == BluetoothState.STATE_CONNECTED) {
                    Log.d(TAG, "State connected!!");
                    // Do something when successfully connected
                } else if (state == BluetoothState.STATE_CONNECTING) {
                    // Do something while connecting
                    Log.d(TAG, "State connecting!!");
                } else if (state == BluetoothState.STATE_LISTEN) {
                    // Do something when device is waiting for connection
                    Log.d(TAG, "State listen");
                } else if (state == BluetoothState.STATE_NONE) {
                    // Do something when device don't have any connection
                    Log.d(TAG, "State none");
                }
            }
        });
    }

    @OnClick(R.id.ivSave)
    public void saveEcg() {
        if (mChart.saveToGallery("title" + System.currentTimeMillis(), 50)) {
            Toast.makeText(getApplicationContext(), "Saving SUCCESSFUL!",
                    Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getApplicationContext(), "Saving FAILED!", Toast.LENGTH_SHORT)
                    .show();
    }

}
